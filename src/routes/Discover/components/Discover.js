import React, { Component } from 'react';
import DiscoverBlock from './DiscoverBlock/components/DiscoverBlock';
import '../styles/_discover.scss';
import Config from '../../../config.js';
import Request from './Request';

export default class Discover extends Component {
  constructor() {
    super();

    this.state = {
      newReleases: [],
      playlists: [],
      categories: [],
      access_token: null,
      token_type: null
    };
  }

  render() {
    const { newReleases, playlists, categories } = this.state;

    return (
      <div className="discover">
        <DiscoverBlock text="RELEASED THIS WEEK" id="released" data={newReleases} />
        <DiscoverBlock text="FEATURED PLAYLISTS" id="featured" data={playlists} />
        <DiscoverBlock text="BROWSE" id="browse" data={categories} imagesKey="icons" />
      </div>
    );
  }

  componentDidMount(){
    this.getToken();
  }

  getToken = () => {
    const params = new URLSearchParams();
    params.append('grant_type', 'client_credentials');

    Request({
      url: Config.api.authUrl,
      method: 'POST',
      data: params,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      auth: {
        username: Config.api.clientId,
        password: Config.api.clientSecret
      }
    }).then((success) => {
      this.setState({
        access_token: success.data.access_token,
        token_type: success.data.token_type
      }, () => {
        this.getData();
      });
    });
  }

  getData = () => {
    const rests = [{
      url: 'new-releases',
      data: 'newReleases',
      response: 'albums'
    }, {
      url: 'featured-playlists',
      data: 'playlists',
      response: 'playlists'
    }, {
      url: 'categories',
      data: 'categories',
      response: 'categories' 
    }];

    rests.forEach((item) => {
      Request({
        url: `${ Config.api.baseUrl }/browse/${ item.url }?country=CO`,
        headers: {
          Authorization: `${ this.state.token_type } ${ this.state.access_token }`
        }
      }).then((success) => {
        this.setState({
          [item.data]: success.data[item.response].items
        });
      });
    });
  }
}
