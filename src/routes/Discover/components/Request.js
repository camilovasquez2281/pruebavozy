const axios = require('axios');

const Request = async (options) => {
	return await axios(options.url, {
	  	method: options.method || 'GET',
	  	headers: options.headers || {},
	  	data: options.data || null,
	  	auth: options.auth || null
	}).then((response) => {
		return response;
	}).catch((error) => {
		throw error.response;
	});
}

export default Request;